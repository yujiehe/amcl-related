# amcl_lib解读

> 主要对`/include/amcl`以及`/src/amcl`中的三个库函数进行联合分析

## I. /map



&nbsp;

&nbsp;

## II. /pf



&nbsp;

&nbsp;

## III. /sensors

在amcl的节点中，传感器数据有激光传感器数据，里程计传感器数据。传感器的数据结构主要有三个数据类型进行存储。

- AMCLSensorData
- AMCLLaserData
- AMCLOdomData

这三个数据结构分别定义在amcl_sersor.h, amcl_odom.h 和 amcl_laser.h的头文件中。
其中：
AMCLLaserData和AMCLOdomData都共有继承了AMCLSensorData

### 3.1 sensor

#### 基本信息

AMCLSensorData的数据结构

```c++
// include/amcl/amcl_sensor.h

class AMCLSensorData
{
  // 生成AMCLSensor的指针
  // Pointer to sensor that generated the data
  public: AMCLSensor *sensor;
          // 析构函数定义成了一个虚函数
          virtual ~AMCLSensorData() {}

  // 包含了一个时间戳
  // Data timestamp
  public: double time;
};
```

#### 功能

进行相关传感器与模型的初始化与更新

#### 代码关键

数据结构的相关声明

```c++
// include/amcl/amcl_sensor.h

class AMCLSensor
{
  // 构造函数
  // Default constructor
  public: AMCLSensor();
         
  // 析构函数同样定义为一个虚函数
  // Default destructor
  public: virtual ~AMCLSensor();

  // 基于运动模型更新滤波器，如果滤波器已经更新就返回true
  // Update the filter based on the action model.  Returns true if the filter
  // has been updated.
  public: virtual bool UpdateAction(pf_t *pf, AMCLSensorData *data);

  // 根据传感器数据模型初始化滤波器，如果滤波器已经被更新就返回true
  // Initialize the filter based on the sensor model.  Returns true if the
  // filter has been initialized.
  public: virtual bool InitSensor(pf_t *pf, AMCLSensorData *data);
  
  // 根据传感器数据模型更新滤波器，如果滤波器已经被更新就返回true
  // Update the filter based on the sensor model.  Returns true if the
  // filter has been updated.
  public: virtual bool UpdateSensor(pf_t *pf, AMCLSensorData *data);

  // Flag is true if this is the action sensor
  public: bool is_action;

  // Action pose (action sensors only)
  public: pf_vector_t pose;

  // AMCL Base
  //protected: AdaptiveMCL & AMCL;
};
```

&nbsp;

### 3.2 odom

#### 基本信息

AMCLOdomData的数据结构

```c++
// include/amcl/amcl_odom.h

// 里程计数据结构为共有继承AMCLSensorData
// Odometric sensor data
class AMCLOdomData : public AMCLSensorData
{
  // pose里程计的位姿
  // Odometric pose
  public: pf_vector_t pose;

  // delta为里程计位姿的改变
  // Change in odometric pose
  public: pf_vector_t delta;
};
```

#### 功能

进行里程计模型的初始化与更新

#### 代码关键

- 参数

如果〜odom_model_type是“diff”，那么我们使用**Probabilistic Robotics** 的**sample_motion_model_odometry**算法，p134~139; 这个模型使用噪声参数odom_alpha_1到odom_alpha4。

如果〜odom_model_type是“omni”，那么我们使用一个自定义模型用于一个全方位的基础，它使用odom_alpha_1到odom_alpha_5。前四个参数的含义与“diff”模型的含义相似。第五个参数捕获机器人垂直于观察到的行进方向平移（不旋转）的倾向。

发现并修复了一个错误。但是固定旧模型将改变或破坏已经调谐的机器人系统的本地化，因此新的固定里程计模型被添加为新类型“差分校正”和“全向校正”。odom_alpha参数的默认设置只适合旧模型，对于新模型，这些值可能需要更小，请参阅[ROS Answer Tuning AMCL's diff-corrected and omni-corrected odom models](https://answers.ros.org/question/227811/tuning-amcls-diff-corrected-and-omni-corrected-odom-models/)。

以下为参数含义解读与图示：

```
~odom_model_type (string, default: "diff")
模型类型（"diff", "omni", "diff-corrected" or"omni-corrected"）
diff：2轮差分    omni：全向轮，y方向有速度，后两者为修正模型

~odom_alpha1 (double, default: 0.2)
指定里程旋转估计（机器人运动旋转分量）中期望的噪声
~odom_alpha2 (double, default: 0.2)
指定里程旋转估计（机器人运动位移分量）中期望的噪声
~odom_alpha3 (double, default: 0.2)
指定里程位移估计（来自机器人运动位移分量）中期望的噪声
~odom_alpha4 (double, default: 0.2)
指定里程位移估计（来自机器人运动旋转分量）期望噪声
~odom_alpha5 (double, default: 0.2)
位移相关噪声参数 (only used if model is"omni").
```

~odom_alpha1 (double, default: 0.2)  机器人在转角分量的运动噪声  增大该值，机器人发生有旋转运动时，就会出现扇形噪声粒子云

![alpha1](./pic/odom/alpha1)

~odom_alpha2 (double, default: 0.2)    机器人在横向分量运动噪声，噪声在机器人左右两边分布 0.5如下图

![alpha2](./pic/odom/alpha2)

~odom_alpha3 (double, default: 0.2) 改参数是纵向分量运动噪声，沿着机器人前进方向分布 

![alpha3](./pic/odom/alpha3)

~odom_alpha4 (double, default: 0.2)  斜角方向上的运动噪声 

![alpha4](./pic/odom/alpha4)

~odom_alpha5 (double, default: 0.2)  第五个参数 对于2轮差分diff 里程计模型无用，可忽略  该参数只对全向运动模型有用，

- 代码实现

以下部分主要以最为简化的`ODOM_MODEL_DIFF`进行分析，其余模型只是在本片段进行深入一步的延伸：s

```c++
  case ODOM_MODEL_DIFF:
  {
    // Implement sample_motion_odometry (Prob Rob p 136)
    double delta_rot1, delta_trans, delta_rot2;
    double delta_rot1_hat, delta_trans_hat, delta_rot2_hat;
    double delta_rot1_noise, delta_rot2_noise;

    // Avoid computing a bearing from two poses that are extremely near each
    // other (happens on in-place rotation).
    if(sqrt(ndata->delta.v[1]*ndata->delta.v[1] + 
            ndata->delta.v[0]*ndata->delta.v[0]) < 0.01)
      delta_rot1 = 0.0;
    else
      delta_rot1 = angle_diff(atan2(ndata->delta.v[1], ndata->delta.v[0]),
                              old_pose.v[2]);
    delta_trans = sqrt(ndata->delta.v[0]*ndata->delta.v[0] +
                       ndata->delta.v[1]*ndata->delta.v[1]);
    delta_rot2 = angle_diff(ndata->delta.v[2], delta_rot1);

    // We want to treat backward and forward motion symmetrically for the
    // noise model to be applied below.  The standard model seems to assume
    // forward motion.
    delta_rot1_noise = std::min(fabs(angle_diff(delta_rot1,0.0)),
                                fabs(angle_diff(delta_rot1,M_PI)));
    delta_rot2_noise = std::min(fabs(angle_diff(delta_rot2,0.0)),
                                fabs(angle_diff(delta_rot2,M_PI)));

    for (int i = 0; i < set->sample_count; i++)
    {
      pf_sample_t* sample = set->samples + i;

      // Sample pose differences
      delta_rot1_hat = angle_diff(delta_rot1,
                                  pf_ran_gaussian(this->alpha1*delta_rot1_noise*delta_rot1_noise +
                                                  this->alpha2*delta_trans*delta_trans));
      delta_trans_hat = delta_trans - 
              pf_ran_gaussian(this->alpha3*delta_trans*delta_trans +
                              this->alpha4*delta_rot1_noise*delta_rot1_noise +
                              this->alpha4*delta_rot2_noise*delta_rot2_noise);
      delta_rot2_hat = angle_diff(delta_rot2,
                                  pf_ran_gaussian(this->alpha1*delta_rot2_noise*delta_rot2_noise +
                                                  this->alpha2*delta_trans*delta_trans));

      // Apply sampled update to particle pose
      sample->pose.v[0] += delta_trans_hat * 
              cos(sample->pose.v[2] + delta_rot1_hat);
      sample->pose.v[1] += delta_trans_hat * 
              sin(sample->pose.v[2] + delta_rot1_hat);
      sample->pose.v[2] += delta_rot1_hat + delta_rot2_hat;
    }
  }
```

&nbsp;

### 3.3 laser

#### 基本信息

AMCLLaserData的数据结构

```c++
// include/amcl/amcl_laser.h

// 与odom一样公有继承AMCLSensorData的类
// Laser sensor data
class AMCLLaserData : public AMCLSensorData
{
  public:
    AMCLLaserData () {ranges=NULL;};
    virtual ~AMCLLaserData() {delete [] ranges;};
  // 雷达参数
  // Laser range data (range, bearing tuples)
  public: int range_count;
  public: double range_max;
  public: double (*ranges)[2];
};
```

#### 功能

进行雷达模型的初始化与更新

#### 代码关键